import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

import javax.imageio.ImageIO;

public class Main
{

	public static final File OUTPUT_FILE = new File("./fonts/");
	public static final int FIRST_CHARACTER = 32;
	public static final int CHARACTERS = 95;
	public static final int BMP_WIDTH = 256;

	public static void main(String[] args)
	{
		if (args.length >= 2)
		{
			String fontName = args[0];
			int fontSize = Integer.parseInt(args[1]);

			Set<Character> options = new TreeSet<Character>();

			for (int i = 0; i < args.length; i++)
			{
				if (args[i].startsWith("-"))
				{
					String optionsString = args[i].substring(1);
					for (char c : optionsString.toCharArray())
						options.add(c);
				}
			}

			generateFont(fontName, fontSize, options);
		} else
		{
			System.err.println("Invalid args. Correct usage: fg <font_name> <font_size>");
		}
	}

	public static void generateFont(String fontName, int fontSize, Set<Character> options)
	{
		// The character is placed at (x,y) the next character is placed at (x+w,y)

		String fileName = fontName + fontSize;

		for (char c : options)
			fileName += c;

		File bmpFile = new File(OUTPUT_FILE, fileName + ".bmp");
		File fntFile = new File(OUTPUT_FILE, fileName + ".fnt");
		Glyph[] glyphs = new Glyph[CHARACTERS];
		int baselineX = 0;
		int baselineY = 0;
		int lineHeight;
		int ascent;
		int descent;
		int leading;

		int fontStyle = 0;

		if (options.contains('b'))
		{
			fontStyle |= Font.BOLD;
		} else if (options.contains('i'))
		{
			fontStyle |= Font.ITALIC;
		} else
		{
			fontStyle = Font.PLAIN;
		}

		Font font = new Font(fontName, fontStyle, fontSize);
		BufferedImage canvas = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = (Graphics2D) canvas.getGraphics();
		FontMetrics metrics;
		g.setFont(font);

		if (options.contains('a'))
		{
			g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		} else
		{
			g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
		}

		metrics = g.getFontMetrics();

		ascent = metrics.getMaxAscent();
		descent = metrics.getMaxDescent();
		leading = metrics.getLeading();
		lineHeight = ascent + descent + leading;

		// Determine the location of the font glyphs
		for (int glyph = 0; glyph < CHARACTERS; glyph++)
		{
			Glyph currentGlyph = new Glyph();
			currentGlyph.codepoint = glyph + FIRST_CHARACTER;
			currentGlyph.advanceWidth = metrics.charWidth(currentGlyph.codepoint);
			currentGlyph.charWidth = (int) metrics.getStringBounds(Character.toString((char) currentGlyph.codepoint), g).getWidth();

			if (baselineX + currentGlyph.advanceWidth >= BMP_WIDTH)
			{
				baselineY += lineHeight;
				baselineX = 0;
			}

			currentGlyph.x = baselineX;
			currentGlyph.y = baselineY;
			baselineX += currentGlyph.charWidth;

			System.out.println(((char) currentGlyph.codepoint) + ": (" + currentGlyph.x + ", " + currentGlyph.y + ")");

			glyphs[glyph] = currentGlyph;
		}

		// Render the glyphs to a bitmap
		canvas = new BufferedImage(BMP_WIDTH, baselineY + ascent + descent + metrics.getLeading(), BufferedImage.TYPE_INT_RGB);
		g = (Graphics2D) canvas.getGraphics();
		g.setFont(font);

		if (options.contains('a'))
		{
			g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		} else
		{
			g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
		}

		// Draw black box
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

		// White characters so they can be multiplicatively tinted
		g.setColor(Color.WHITE);

		// Draw the characters as strings
		for (Glyph glyph : glyphs)
		{
			g.drawString(Character.toString((char) glyph.codepoint), glyph.x, glyph.y + ascent);
		}

		if (!OUTPUT_FILE.exists())
			OUTPUT_FILE.mkdir();

		// Output the rendered image
		try
		{

			ImageIO.write(canvas, "BMP", bmpFile);
		} catch (IOException e)
		{
			System.err.println("Failed to generate the bitmap font: ");
			e.printStackTrace();
		}

		// Create the font data file
		try
		{
			DataOutputStream fntOS = new DataOutputStream(new FileOutputStream(fntFile));

			// Serialize the font meta.

			// Write font name as ascii string.
			fntOS.writeInt(fontName.length());
			for (char c : fontName.toCharArray())
				fntOS.writeByte((int) c);

			fntOS.writeInt(fontSize);
			fntOS.writeInt(ascent);
			fntOS.writeInt(descent);
			fntOS.writeInt(leading);

			// Serialize the glyph data.
			for (Glyph glyph : glyphs)
			{
				fntOS.writeInt(glyph.codepoint);
				fntOS.writeInt(glyph.x);
				fntOS.writeInt(glyph.y);
				fntOS.writeInt(glyph.advanceWidth);
				fntOS.writeInt(glyph.charWidth);
			}

			fntOS.close();
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}

	}

}
